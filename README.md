---
SPDX-License-Identifier: CC-BY-SA-4.0
SPDX-FileCopyrightText: © 2022 Olaf Meeuwissen
---

# Starter Kit

[![standard-readme compliant](https://img.shields.io/badge/readme%20style-standard-brightgreen.svg)](https://github.com/RichardLitt/standard-readme#readme)

Get going on a new software or documentation project without a lot of
hassle.

# Install

``` sh
git clone --origin starter-kit https://gitlab.com/paddy-hack/starter-kit.git
cd starter-kit
```

# Usage

``` sh
script/setup
```

# Contributing

Just [open an issue](https://gitlab.com/paddy-hack/starter-kit/-/issues).

# License

[CC-BY-SA-4.0](https://creativecommons.org/licenses/by-sa/4.0/) © [Olaf Meeuwissen](https://gitlab.com/paddy-hack)
